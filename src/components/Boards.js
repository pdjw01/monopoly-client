import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { useAuth } from "../context/auth";
import { Redirect } from "react-router-dom";
import { Error, Button } from "./AuthForms";

const Boards = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState([]);
  const [boardFiles, setBoardFiles] = useState([]);
  const [boardFile, setBoardFile] = useState([]);
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const [showTok, setShowTok] = useState(false);
  const [showTitle, setShowTitle] = useState(false);
  const [showChance, setShowChance] = useState(false);
  const [showCommunityChest, setShowCommunityChest] = useState(false);

  useEffect(() => {
    setReload(false);
    const getBoards = async () => {
      try {
        const result = await BoardService.getBoards(authJWT.jwt);
        if (result && result.status===200) {
          setError();
          setContent(result.data)
        }
        const files = await BoardService.getBoardFiles(authJWT.jwt);
        if (files && files.status===200) {
          const filenames = files.data.filter((filename) => !result.data.some((loaded) => loaded.filename===filename ))
          setError();
          setBoardFiles(filenames)
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getBoards();
  }, [authJWT.jwt, reload]);

  const changeBoardFile = (e) => {
    if (e.target.value === '0') {
      document.getElementById("addBoard").disabled = true;
    } else {
      document.getElementById("addBoard").disabled = false;
      setBoardFile(e.target.value)
    }
  };

  const addBoardFromFile = async(e) => {
    e.preventDefault();  
    try{
      const result = await BoardService.addBoardFile(boardFile, authJWT.jwt);
      if (result && result.status===201) {
        setError();
        setReload(true);
      } else {
        setError("Cannot add board");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  const deleteBoard = async(boardid) => {
    try {
      const result = await BoardService.deleteBoard(boardid, authJWT.jwt);
      if (result && result.status===204) {
        setError();
        setReload(true);
      } else {
        setError("Cannot delete board");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  const showTokens = (board) => {
    localStorage.setItem("board", JSON.stringify(board));
    setShowTok(true);
  }

  const showTitleDeeds = (board) => {
    localStorage.setItem("board", JSON.stringify(board));
    setShowTitle(true);
  };

  const showChances = (board) => {
    localStorage.setItem("board", JSON.stringify(board));
    setShowChance(true);
  };

  const showCommunityChests = (board) => {
    localStorage.setItem("board", JSON.stringify(board));
    setShowCommunityChest(true);
  };

  if (showTok) {
    return(
      <Redirect 
        to={{ pathname: "/tokens", state: { referer: props.location } }}
      />
    );
  };

  if (showTitle) {
    return(
      <Redirect 
        to={{ pathname: "/titleDeeds", state: { referer: props.location } }}
      />
    );
  };

  if (showChance) {
    return(
      <Redirect 
        to={{ pathname: "/chances", state: { referer: props.location } }}
      />
    );
  };

  if (showCommunityChest) {
    return(
      <Redirect 
        to={{ pathname: "/communityChests", state: { referer: props.location } }}
      />
    );
  };

  if (content || boardFiles) {
    return(
        <div className="container">
        <header className="jumbotron">
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Version</th>
                <th scope="col">Currency</th>
                <th scope="col">Bank Float</th>
                <th scope="col">Houses</th>
                <th scope="col">Hotels</th>
                <th scope="col">Salary</th>
                <th scope="col">First pay</th>
                <th scope="col">Tokens</th>
                <th scope="col">Title deeds</th>
                <th scope="col">Chance cards</th>
                <th scope="col">Community chest cards</th>
                {((authJWT.permissionLevel & 12) > 0) && (
                  <th scope="col">Delete</th>
                )}
              </tr>
            </thead>
            <tbody>
              {content.map((board) => {
                return(
                  <tr key={board._id}>
                    <td>{board.version}</td>
                    <td>{board.currency}</td>
                    <td>{board.bankFloat}</td>
                    <td>{board.houses}</td>
                    <td>{board.hotels}</td>
                    <td>{board.salary}</td>
                    <td>{board.firstPay}</td>
                    <td>
                      <Button onClick={() => {showTokens(board)}}>Tokens</Button>
                    </td>
                    <td>
                      <Button onClick={() => {showTitleDeeds(board)}}>Title Deeds</Button>
                    </td>
                    <td>
                      <Button onClick={() => {showChances(board)}}>Chance Cards</Button>
                    </td>
                    <td>
                      <Button onClick={() => {showCommunityChests(board)}}>Community Chest Cards</Button>
                    </td>
                    {((authJWT.permissionLevel & 12) > 0) && (
                      <td>
                        <Button onClick={() => {deleteBoard(board._id)}}>Delete</Button>
                      </td>
                    )}
                    </tr>
                )
              })}
            </tbody>
          </table>
          {((authJWT.permissionLevel & 12) > 0) && (
            <form onSubmit={addBoardFromFile}>
              <label>Add new board: </label>
              <select id='newBoard' defaultValue='0' onChange={changeBoardFile}>
                <option value='0' key='noFile'>Select file</option>
              {boardFiles.map((file) => {
                return (
                  <option value={file} key={file}>{file}</option>
                )
              })}
              </select>
              <button id='addBoard' type="submit" disabled>Add</button>
            </form>
          )}
        </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  };
};

export default Boards;