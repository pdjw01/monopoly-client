const auth_header = () => {
  const API_URL = (process.env.NODE_ENV === "production" && "https://ancient-shelf-85727.herokuapp.com/api/") || "http://localhost:4000/api/";

  const authHeader = (jwt) => {
    return { headers: { Authorization: 'Bearer ' + jwt }};
  }

  return {
    authHeader, API_URL
  };
}
module.exports = auth_header;