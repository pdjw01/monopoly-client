import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import { useAuth } from "../context/auth";
import { Redirect } from "react-router-dom";
import { Error } from "./AuthForms";

const Users = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState();
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const [changePass, setChangePass] = useState(false);
  const [changePermissionLevels, setChangePermissionLevels] = useState(false);

  useEffect(() => {
    setReload(false);
    const getUsers = async () => {
      try {
        const result = await UserService.getUsers(authJWT.jwt);
        if (result && result.status===200) {
          setContent(result.data);
          setError();
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getUsers();
  }, [authJWT.jwt, reload]);

  const deleteUser = async(userid) => {
    try {
      const result = await UserService.deleteUser(userid, authJWT.jwt);
      if (result && result.status===204) {
        setReload(true);
        setError();
      } else {
        setError("Cannot delete user");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  }

  const changePassword = (user) => {
    localStorage.setItem("display", JSON.stringify(user));
    setChangePass(true);
  }

  const changeRoles = (user) => {
    localStorage.setItem("display", JSON.stringify(user));
    setChangePermissionLevels(true);
  }

  if (changePass) {
    return(
      <Redirect 
        to={{ pathname: "/password", state: { referer: props.location } }}
      />
    );
  }

  if (changePermissionLevels) {
    return(
      <Redirect 
        to={{ pathname: "/roles", state: { referer: props.location } }}
      />
    );
  }

  if (content) {
    return(
        <div className="container">
        <header className="jumbotron">
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">username</th>
                <th scope="col">Player</th>
                <th scope="col">Enhanced</th>
                <th scope="col">Admin</th>
                <th scope="col">Superuser</th>
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Change Roles</th>
                )}
                <th scope="col">Change Password</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>
              {content.map((user) => {
                user.roles = UserService.permission2roles(user.permissionLevel)
                return(
                  <tr key={user._id}>
                    <td>{user.username}</td>
                    <td>{user.roles.includes('Player') && "Player"}</td>
                    <td>{user.roles.includes('Enhanced') && "Enhanced"}</td>
                    <td>{user.roles.includes('Admin') && "Admin"}</td>
                    <td>{user.roles.includes('Superuser') && "Superuser"}</td>
                    {((authJWT.permissionLevel & 8) > 0) && (
                      <td>
                        <button onClick={() => {changeRoles(user)}}>Change Roles</button>
                      </td>
                    )}
                    <td>
                      <button onClick={() => {changePassword(user)}}>Change Password</button>
                    </td>
                    <td>
                      <button onClick={() => {deleteUser(user._id)}}>Delete</button>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  }
}

export default Users;