import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Card, Error, Input, Button } from "./AuthForms";

const Token = (props) => {
  const { authJWT } = useAuth();
  const token = JSON.parse(localStorage.getItem("token"));
  const [tokenName, setTokenName] = useState(token.tokenName);
  const [filename, setFilename] = useState(token.filename);
  const [image, setImage] = useState(token.image);
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const referer = (props.location.state && props.location.state.referer) || '/tokens';
  const [imageFiles, setImageFiles] = useState([]);

  useEffect(() => {
    setReload(false);
    const getImages = async () => {
      try {
        if (!image) {
          const file = await BoardService.getTokenImage(filename, authJWT.jwt);
          if (file && file.status===200) {
            setError();
            setImage(file.data);
          }  
        }
        if (!imageFiles || imageFiles.length === 0) {
          const files = await BoardService.getTokenImages(authJWT.jwt);
          if (files && files.status===200) {
            setError();
            setImageFiles(files.data);
          }
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getImages();
  }, [authJWT.jwt, reload, image, filename, imageFiles]);

  const editFilename = async (file) => {
    try {
      const result = await BoardService.getTokenImage(file, authJWT.jwt);
      if (result && result.status===200) {
        setFilename(file)
        setImage(result.data)
        setError();
      }
  } catch (err) {
      setError(UserService.handleError(err));
    }
  }

  const addToken = async () => {
    if (tokenName.length === 0 ) {setError("Token Name is required")}
    else if (filename.length === 0 ) {setError("Filename is required")}
    else {
      try {
        token.tokenName = tokenName;
        token.filename = filename;
        if (token._id) {
          const result = await BoardService.putToken(token, authJWT.jwt);
          if (result && (result.status === 200)) {
            setReload(true);
            setError();
          } else {
            setError((result.body && result.body.errors) || "Could not modify Token");
          }
        } else {
          const result = await BoardService.addToken(token, authJWT.jwt);
          if (result && (result.status === 201)) {
            setReload(true);
            setError();
          } else {
            setError((result.body && result.body.errors) || "Could not add Token");
          }  
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  };

  if (reload) {
    return <Redirect to={referer} />;
  };

  return(
    <Card>
      { token._id ? (
      <h3>Token&nbsp;{token._id}</h3>
      ) : (
        <h3>Add New Token</h3>
      )}
    { error && <Error>{error}</Error> }
        <table>
          <tbody>
            <tr>
              <td>Property Name</td>
              <td>
                <Input
                  type="text"
                  value={tokenName}
                  onChange={e => {
                    setTokenName(e.target.value);
                  }}
                  placeholder={tokenName}
                />
              </td>
            </tr>
            <tr>
              <td>Filename</td>
              <td>
                <select defaultValue={filename} 
                  value={filename} 
                  onChange={e => {
                    editFilename(e.target.value);
                  }}>
                {imageFiles.map((file) => {
                  return (
                    <option value={file} key={file}>{file}</option>
                  )
                })}
                </select>
              </td>
            </tr>
            <tr>
              <td>Image</td>
              <td><img src={`data:image/png;base64,${image}`} alt={tokenName}/></td>
            </tr>
          </tbody>
        </table>
        <Button onClick={() => {addToken()}}>OK</Button>
      </Card>
  )
};

export default Token;