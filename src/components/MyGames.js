import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { useAuth } from "../context/auth";
import { Error, Button, Input } from "./AuthForms";

const MyGames = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState([]);
  const [newGameName, setNewGameName] = useState('');
  const [boards, setBoards] = useState([]);
  const [board, setBoard] = useState('0');
  const [tokens, setTokens] = useState([]);
  const [token, setToken] = useState();
  const [tokenImage, setTokenImage] = useState();
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);

  useEffect(() => { 
    setReload(false);
    const getGames = async () => {
      try {
        document.getElementById("addGame").disabled = true;
        const boards = await BoardService.getBoards(authJWT.jwt);
        if (boards && boards.status===200) {
          setError();
          setBoards(boards.data);
        }
        const players = await BoardService.getMyPlayers(authJWT.userid, authJWT.jwt);
        if (players && players.status===200) {
          const myGames = await Promise.all(players.data.map(async (myGame) => {
            const game = await BoardService.getGame(myGame.game, authJWT.jwt);
            if (game && game.status===200) {
              myGame.gameName = game.data.gameName;
              myGame.board = game.data.board;
              myGame.status = game.data.status;
            } else {
              setError('game error');
            }
            const token = await BoardService.getToken(myGame.token, authJWT.jwt);
            if (token && token.status===200) {
              myGame.tokenName = token.data.tokenName;
              myGame.tokenImage = token.data.image;
            } else {
              setError('token error');
            }
            myGame.boardName = boards.data.find(element => element._id === myGame.board).version
            return myGame;
          }));
          setContent(myGames);
        } else {
          setError('player error');
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getGames();
  }, [authJWT.userid, authJWT.jwt, reload]);

  const changeBoard = async(e) => {
    setToken();
    setTokenImage();
    document.getElementById("addGame").disabled = true;
    if (e.target.value === '0') {
      setError();
      setTokens([]);
      setBoard();
    } else {
      setBoard(e.target.value)
      try {
        const tokens = await BoardService.getTokensOnBoard(e.target.value, authJWT.jwt);
        if (tokens && tokens.status===200) {
          setError();
          setTokens(tokens.data);
        }  
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  };

  const changeToken = (e) => {
    if (e.target.value === '0') {
      document.getElementById("addGame").disabled = true;
      setToken();
      setTokenImage();
    } else if (newGameName.length === 0) {
        document.getElementById("addGame").disabled = true;
        setToken(e.target.value);
        setTokenImage(tokens.find((token) => token._id === e.target.value).image);
      } else {
        document.getElementById("addGame").disabled = false;
      setToken(e.target.value);
      setTokenImage(tokens.find((token) => token._id === e.target.value).image);
    }
  };

  const changeGameName = (e) => {
    setNewGameName(e.target.value)
    if (e.target.value.length === 0 || !token) {
      document.getElementById("addGame").disabled = true;
    } else {
      document.getElementById("addGame").disabled = false;
    }
  };

  const addNewGame = async() => {
    try{
      const result = await BoardService.addGame(newGameName, board, authJWT.userid, token, authJWT.jwt);
      if (result && result.status===201) {
        setError();
        setTokenImage();
        setToken('0');
        setBoard('0');
        setReload(true);
      } else {
        setError("Cannot add game");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  const leaveGame = async(playerid) => {
    try {
      const result = await BoardService.deletePlayer(playerid, authJWT.jwt);
      if (result && result.status===204) {
        setError();
        setReload(true);
      } else {
        setError("Cannot leave game");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  const deleteGame = async(gameid) => {
    try {
      const result = await BoardService.deleteGame(gameid, authJWT.jwt);
      if (result && result.status===204) {
        setError();
        setReload(true);
      } else {
        setError("Cannot delete game");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  if (content) {
    return(
        <div className="container">
        <header className="jumbotron">
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Game Name</th>
                <th scope="col">Board</th>
                <th scope="col">Token</th>
                <th scope="col">Image</th>
                <th scope="col">Status</th>
                <th scope="col">Leave</th>
                {((authJWT.permissionLevel & 12) > 0) && (
                  <th scope="col">Delete</th>
                )}
              </tr>
            </thead>
            <tbody>
              {content.map((game) => {
                return(
                  <tr key={game._id}>
                    <td>{game.gameName}</td>
                    <td>{game.boardName}</td>
                    <td>{game.tokenName}</td>
                    <td><img src={`data:image/png;base64,${game.tokenImage}`} alt={game.tokenName}/></td>
                    <td>{game.status}</td>
                    <td>
                        <Button onClick={() => {leaveGame(game._id)}}>Leave Game</Button>
                      </td>
                    {((authJWT.permissionLevel & 12) > 0) && (
                      <td>
                        <Button onClick={() => {deleteGame(game.game)}}>Delete</Button>
                      </td>
                    )}
                    </tr>
                )
              })}
              <tr key="createGame">
                <td>
                  <Input
                    type="text"
                    value={newGameName}
                    onChange={changeGameName}
                    placeholder="newGameName"
                  />
                </td>
                <td>
                  <select id='newBoard' defaultValue='0' onChange={changeBoard}>
                    <option value='0' key='noBoard'>Select board</option>
                    {boards.map((board) => {
                      return (
                        <option value={board._id} key={board._id}>{board.version}</option>
                      )
                    })}
                  </select>
                </td>
                <td>
                  <select id='newToken' defaultValue='0' value={token} onChange={changeToken}>
                    <option value='0' key='noToken'>Select token</option>
                    {tokens.map((token) => {
                      return (
                        <option value={token._id} key={token._id}>{token.tokenName}</option>
                      )
                    })}
                    </select>
                </td>
                <td>
                  {tokenImage && (<img src={`data:image/png;base64,${tokenImage}`} alt={token}/>)}
                </td>
                <td>
                  <button id='addGame' onClick={() => addNewGame()}>Add Game</button>
                </td>
              </tr>
            </tbody>
          </table>
        </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  };
};

export default MyGames;