import React, {useState} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Card, Error, Input, Button } from "./AuthForms";

const TitleDeed = (props) => {
  const { authJWT } = useAuth();
  const titleDeed = JSON.parse(localStorage.getItem("titleDeed"));
  const [propertyName, setPropertyName] = useState(titleDeed.propertyName);
  const [group, setGroup] = useState(titleDeed.group);
  const [type, setType] = useState(titleDeed.type);
  const [boardPos, setBoardPos] = useState(titleDeed.boardPos);
  const [salePrice, setSalePrice] = useState(titleDeed.salePrice);
  const [housePrice, setHousePrice] = useState(titleDeed.housePrice);
  const [mortgageValue, setMortgageValue] = useState(titleDeed.mortgageValue);
  const [rentZero, setRentZero] = useState(titleDeed.rent.zero);
  const [rentOne, setRentOne] = useState(titleDeed.rent.one);
  const [rentTwo, setRentTwo] = useState(titleDeed.rent.two);
  const [rentThree, setRentThree] = useState(titleDeed.rent.three);
  const [rentFour, setRentFour] = useState(titleDeed.rent.four);
  const [rentHotel, setRentHotel] = useState(titleDeed.rent.hotel);
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const referer = (props.location.state && props.location.state.referer) || '/titleDeeds';

  const addTitleDeed = async () => {
    if (propertyName.length === 0 ) {setError("Property Name is required")}
    else if (group.length === 0 ) {setError("Group is required")}
    else if (type.length === 0 ) {setError("Type is required")}
    else if (boardPos < 0 || boardPos > 39) {setError("BoardPos should be between 0 and 39")}
    else if (salePrice < 0) {setError("Sale Price should be positive")}
    else if (housePrice < 0) {setError("House Price should be positive")}
    else if (mortgageValue < 0) {setError("Mortgage Value should be positive")}
    else if (rentZero < 0) {setError("RentZero should be positive")}
    else if (rentOne < 0) {setError("RentOne should be positive")}
    else if (rentTwo < 0) {setError("RentTwo should be positive")}
    else if (rentThree < 0) {setError("RentThree should be positive")}
    else if (rentFour < 0) {setError("RentFour should be positive")}
    else if (rentHotel < 0) {setError("RentHotel should be positive")}
    else {
      try {
        titleDeed.propertyName = propertyName;
        titleDeed.group = group;
        titleDeed.type = type;
        titleDeed.boardPos = boardPos;
        titleDeed.salePrice = salePrice;
        titleDeed.housePrice = housePrice;
        titleDeed.mortgageValue = mortgageValue;
        titleDeed.rent.zero = rentZero;
        titleDeed.rent.one = rentOne;
        titleDeed.rent.two = rentTwo;
        titleDeed.rent.three = rentThree;
        titleDeed.rent.four = rentFour;
        titleDeed.rent.hotel = rentHotel;
        if (titleDeed._id) {
          const result = await BoardService.putTitleDeed(titleDeed, authJWT.jwt);
          if (result && (result.status === 200)) {
            setReload(true);
            setError();
          } else {
            setError((result.body && result.body.errors) || "Could not modify Title Deed");
          }
        } else {
          const result = await BoardService.addTitleDeed(titleDeed, authJWT.jwt);
          if (result && (result.status === 201)) {
            setReload(true);
            setError();
          } else {
            setError((result.body && result.body.errors) || "Could not add Title Deed");
          }  
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  };

  if (reload) {
    return <Redirect to={referer} />;
  };

  return(
    <Card>
        { titleDeed._id ? (
        <h3>titleDeed&nbsp;{titleDeed._id}</h3>
        ) : (
          <h3>Add New Title Deed</h3>
        )}
      { error && <Error>{error}</Error> }
          <table>
            <tbody>
              <tr>
                <td>Property Name</td>
                <td>
                  <Input
                    type="text"
                    value={propertyName}
                    onChange={e => {
                      setPropertyName(e.target.value);
                    }}
                    placeholder={propertyName}
                  />
                </td>
              </tr>
              <tr>
                <td>Group</td>
                <td>
                  <Input
                    type="text"
                    value={group}
                    onChange={e => {
                      setGroup(e.target.value);
                    }}
                    placeholder={group}
                  />
                </td>
              </tr>
              <tr>
                <td>Type</td>
                <td>
                  <Input
                    type="text"
                    value={type}
                    onChange={e => {
                      setType(e.target.value);
                    }}
                    placeholder={type}
                  />
                </td>
              </tr>
              <tr>
                <td>Board Position</td>
                <td>
                  <Input
                    type="number" min="0" step="1" max="39"
                    value={boardPos}
                    onChange={e => {
                      setBoardPos(e.target.value);
                    }}
                    placeholder={boardPos}
                  />
                </td>
              </tr>
              <tr>
                <td>Sale Price</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={salePrice}
                    onChange={e => {
                      setSalePrice(e.target.value);
                    }}
                    placeholder={salePrice}
                  />
                </td>
              </tr>
              <tr>
                <td>House Price</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={housePrice}
                    onChange={e => {
                      setHousePrice(e.target.value);
                    }}
                    placeholder={housePrice}
                  />
                </td>
              </tr>
              <tr>
                <td>Mortgage Value</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={mortgageValue}
                    onChange={e => {
                      setMortgageValue(e.target.value);
                    }}
                    placeholder={mortgageValue}
                  />
                </td>
              </tr>
              <tr>
                <td>Rent Zero</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={rentZero}
                    onChange={e => {
                      setRentZero(e.target.value);
                    }}
                    placeholder={rentZero}
                  />
                </td>
              </tr>
              <tr>
                <td>Rent One</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={rentOne}
                    onChange={e => {
                      setRentOne(e.target.value);
                    }}
                    placeholder={rentOne}
                  />
                </td>
              </tr>
              <tr>
                <td>Rent Two</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={rentTwo}
                    onChange={e => {
                      setRentTwo(e.target.value);
                    }}
                    placeholder={rentTwo}
                  />
                </td>
              </tr>
              <tr>
                <td>Rent Three</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={rentThree}
                    onChange={e => {
                      setRentThree(e.target.value);
                    }}
                    placeholder={rentThree}
                  />
                </td>
              </tr>
              <tr>
                <td>Rent Four</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={rentFour}
                    onChange={e => {
                      setRentFour(e.target.value);
                    }}
                    placeholder={rentFour}
                  />
                </td>
              </tr>
              <tr>
                <td>Rent Hotel</td>
                <td>
                  <Input
                    type="number" min="0" step="1"
                    value={rentHotel}
                    onChange={e => {
                      setRentHotel(e.target.value);
                    }}
                    placeholder={rentHotel}
                  />
                </td>
              </tr>
            </tbody>
          </table>
          <Button onClick={() => {addTitleDeed()}}>OK</Button>
      </Card>
  )
};

export default TitleDeed;