import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import logoImg from "../img/mon1.png";
import { Card, Logo, Form, Input, Button, Error } from "../components/AuthForms";
import { useAuth } from "../context/auth";
import UserService from "../services/UserService"; 

const Password = (props) => {
  const { authJWT } = useAuth();
  const [isChanged, setChanged] = useState(false);
  const [error, setError] = useState("");
  const user = JSON.parse(localStorage.getItem("display"));
  const [password, setPassword] = useState("");
  const referer = (props.location.state && props.location.state.referer) || '/';

  const postChange = async () => {
    if (password.length === 0 ) {setError("Password is required")}
    else {
      try {
        const result = await UserService.changePassword(user._id, password, authJWT.jwt);
        if (result && (result.status === 200)) {
          setError();
          setChanged(true);
        } else {
          setError((result.body && result.body.errors) || "Could not change password");
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  }
 
  if (isChanged) {
    return <Redirect to={referer} />;
  }
  return (
    <Card>
      <Logo src={logoImg} />
      <Form>
      <h3>{user.username}</h3>
        <Input
          type="password"
          value={password}
          onChange={e => {
            setPassword(e.target.value);
          }}
          placeholder="password"
        />
        <Button onClick={postChange}>Change Password</Button>
      </Form>
      { error &&<Error>{error}</Error> }
    </Card>
  );
}

export default Password;