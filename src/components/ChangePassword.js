import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import logoImg from "../img/mon1.png";
import { Card, Logo, Form, Input, Button, Error } from "./AuthForms";
import { useAuth } from "../context/auth";
import UserService from "../services/UserService"; 

const ChangePassword = (props) => {
  const { authJWT } = useAuth();
  const [isChanged, setChanged] = useState(false);
  const [error, setError] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [again, setAgain] = useState("");
  const referer = (props.location.state && props.location.state.referer) || '/';

  const postChange = async () => {
    if (oldPassword.length === 0 ) {setError("Old Password is required")}
    else if (newPassword.length === 0 ) {setError("New Password is required")}
    else if (newPassword !== again ) {setError("Passwords dont match")}
    else {
      try{
        const result = await UserService.login(authJWT.username, oldPassword);
        if (result && (result.status === 200)) {
          try {
            const result = await UserService.changePassword(authJWT.userid, newPassword, authJWT.jwt);
            if (result && (result.status === 200)) {
              setError();
              setChanged(true);
            } else {
              setError((result.body && result.body.errors) || "Could not change password");
            }
          } catch (err) {
            setError(UserService.handleError(err));
          }
        } else {
          setError(`${result.status}: Could not change password`);
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  }
 
  if (isChanged) {
    return <Redirect to={referer} />;
  }
  return (
    <Card>
      <Logo src={logoImg} />
      <Form>
      <h3>{authJWT.username}</h3>
      <Input
          type="password"
          value={oldPassword}
          onChange={e => {
            setOldPassword(e.target.value);
          }}
          placeholder="old password"
        />
        <Input
          type="password"
          value={newPassword}
          onChange={e => {
            setNewPassword(e.target.value);
          }}
          placeholder="new password"
        />
        <Input
          type="password"
          value={again}
          onChange={e => {
            setAgain(e.target.value);
          }}
          placeholder="repeat new password"
        />
        <Button onClick={postChange}>Change Password</Button>
      </Form>
      { error &&<Error>{error}</Error> }
    </Card>
  );
}

export default ChangePassword;