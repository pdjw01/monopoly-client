import axios from 'axios';
import auth_header from './auth-header';
const API_URL = auth_header().API_URL;
const authHeader = auth_header().authHeader;


class UserService {
  handleError(err) {
    if (err.response) {
      // Request made and server responded
      return `${err.response.status}: ${err.response.statusText}: ${err.response.data.errors}`
    } else {
      // Something happened in setting up the request that triggered an Error
      return err.message;
    }
  }

  permission2roles(permissionLevel) {
    const roles = [];
    if ( permissionLevel & 1) roles.push("Player");
    if ( permissionLevel & 2) roles.push("Enhanced");
    if ( permissionLevel & 4) roles.push("Admin");
    if ( permissionLevel & 8) roles.push("Superuser");
    return roles;
  }
  
  addUser(username, password) {
    return axios.post(API_URL + 'users', { username, password });
  }

  login(username, password) {
    return axios.post(API_URL + 'login', { username, password });
  }

  getUserByUserid(userid, jwt) {
    return axios.get(`${API_URL}users/${userid}`, authHeader(jwt));
  }

  getUsers(jwt) {
    return axios.get(API_URL + 'users', authHeader(jwt));
  }

  deleteUser(userid, jwt) {
    return axios.delete(`${API_URL}users/${userid}`, authHeader(jwt));
  }

  changePassword(userid, password, jwt) {
    return axios.patch(`${API_URL}users/${userid}`, { password: password }, authHeader(jwt));
  }

  changeRoles(userid, permissionLevel, jwt) {
    return axios.patch(`${API_URL}login/${userid}`, { permissionLevel: permissionLevel }, authHeader(jwt));
  }

  getUserByUsername(username, jwt) {
    return axios.get(`${API_URL}users?username=${username}`, authHeader(jwt));
  }
}

export default new UserService();