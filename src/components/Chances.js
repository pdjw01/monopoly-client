import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Error, Button } from "./AuthForms";

const Chances = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState([]);
  const board = JSON.parse(localStorage.getItem("board"));
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const [showChance, setShowChance] = useState(false);

  useEffect(() => {
    setReload(false);
    const getChances = async () => {
      try {
        const result = await BoardService.getChancesOnBoard(board._id, authJWT.jwt);
        if (result && result.status===200) {
          setError();
          setContent(result.data)
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getChances();
  }, [board._id, authJWT.jwt, reload]);

  const addChance = () => {
    const chance = {
      board: board._id,
      text: "Chance Text",
      action: "What the chance card will do"
    }
    localStorage.setItem("chance", JSON.stringify(chance));
    setShowChance(true);
  };

  const editChance = (chance) => {
    localStorage.setItem("chance", JSON.stringify(chance));
    setShowChance(true);
  }

  const deleteChance = async(chanceid) => {
    try {
      const result = await BoardService.deleteChance(chanceid, authJWT.jwt);
      if (result && result.status===204) {
        setError();
        setReload(true);
      } else {
        setError("Cannot delete chance");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  if (showChance) {
    return(
      <Redirect 
        to={{ pathname: "/chance", state: { referer: props.location } }}
      />
    );
  };

  if (content) {
    return(
      <div className="container">
        <header className="jumbotron">
          <h3>Chances for {board.version}</h3>
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Text</th>
                <th scope="col">Action</th>
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Edit</th>
                )}
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Delete</th>
                  )}
              </tr>
            </thead>
            <tbody>
              {content.map((chance) => {
                return(
                  <tr key={chance._id}>
                    <td>{chance.text}</td>
                    <td>{chance.action}</td>
                    {((authJWT.permissionLevel & 8) > 0) && (
                    <td>
                      <Button onClick={() => {editChance(chance)}}>Edit</Button>
                    </td>
                    )}
                    {((authJWT.permissionLevel & 8) > 0) && (
                    <td>
                      <Button onClick={() => {deleteChance(chance._id)}}>Delete</Button>
                    </td>
                    )}
                    </tr>
                )
              })}
            </tbody>
          </table>
          {((authJWT.permissionLevel & 8) > 0) && (
          <Button onClick={addChance}>Add</Button>
          )}
          </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  };
};

export default Chances;