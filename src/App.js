import React, { useState } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { AuthContext } from "./context/auth";
import PrivateRoute from './PrivateRoute';

import NavBar from './components/NavBar';
import Home from './components/Home';
import Signup from './components/Signup';
import Login from './components/Login';
import Profile from './components/Profile';
import Users from './components/Users';
import Password from './components/Password';
import Roles from './components/Roles';
import ChangePassword from './components/ChangePassword';
import Boards from './components/Boards';
import Tokens from './components/Tokens';
import Token from './components/Token';
import TitleDeeds from './components/TitleDeeds';
import TitleDeed from './components/TitleDeed';
import Chances from './components/Chances';
import Chance from './components/Chance';
import CommunityChests from './components/CommunityChests';
import CommunityChest from './components/CommunityChest';
import MyGames from './components/MyGames';
import CreatedGames from './components/CreatedGames';

function App(props) {
  const [authJWT, setAuthJWT] = useState();
  
  const setJWT = (data) => {
    localStorage.setItem("jwt", JSON.stringify(data));
    setAuthJWT(data);
  }

  return (
    <AuthContext.Provider value={{ authJWT, setAuthJWT: setJWT }}>
      <Router>
        <NavBar/>
        <Route exact path="/" component={Home} />
        <Route path="/signup" component={Signup} />
        <Route path="/login" component={Login} />
        <PrivateRoute path="/profile" component={Profile} permission="15" />
        <PrivateRoute path="/users" component={Users} permission="12" />
        <PrivateRoute path="/password" component={Password} permission="12" />
        <PrivateRoute path="/roles" component={Roles} permission="12" />
        <PrivateRoute path="/change-password" component={ChangePassword} permission="15" />
        <PrivateRoute path="/boards" component={Boards} permission="12" />
        <PrivateRoute path="/tokens" component={Tokens} permission="12" />
        <PrivateRoute path="/token" component={Token} permission="8" />
        <PrivateRoute path="/titledeeds" component={TitleDeeds} permission="12" />
        <PrivateRoute path="/titledeed" component={TitleDeed} permission="8" />
        <PrivateRoute path="/chances" component={Chances} permission="12" />
        <PrivateRoute path="/chance" component={Chance} permission="8" />
        <PrivateRoute path="/communityChests" component={CommunityChests} permission="12" />
        <PrivateRoute path="/communityChest" component={CommunityChest} permission="8" />
        <PrivateRoute path="/myGames" component={MyGames} permission="15" />
        <PrivateRoute path="/createdGames" component={CreatedGames} permission="15" />
      </Router>
    </AuthContext.Provider>
  );
}


export default App;