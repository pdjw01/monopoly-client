import React from 'react';
import {Link} from 'react-router-dom';
import { useAuth } from "../context/auth";

function NavBar() {
  const { authJWT, setAuthJWT } = useAuth();

   const logOut = () => {
    setAuthJWT();
  }

  return (
    <div>
      <nav className="navbar navbar-expand navbar-dark bg-primary fixed-top">
        <Link className="navbar-brand" to="/">
          RESTful Monopoly
        </Link>
        {(authJWT && ((authJWT.permissionLevel & 12) > 0)) && (
         <div className="navbar-nav mr-auto">
            <li className="nav-item">
            <Link to={"/users"} className="nav-link">
                Users
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/boards"} className="nav-link">
                Boards
              </Link>
            </li>
          </div>
        )}
        {(authJWT && ((authJWT.permissionLevel & 15) > 0)) && (
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/myGames"} className="nav-link">
                My Games
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/createdGames"} className="nav-link">
                Join Game
              </Link>
            </li>
          </div>
        )}
        {authJWT ? (
        <div className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link to={"/profile"} className="nav-link">
            {authJWT.username}
            </Link>
          </li>
          <li className="nav-item">
            <a href="/login" className="nav-link" onClick={logOut}>
              LogOut
            </a>
          </li>
        </div>
        ) : (
        <div className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link to={"/login"} className="nav-link">
              Login
            </Link>
          </li>
          <li className="nav-item">
            <Link to={"/signup"} className="nav-link">
              Sign Up
            </Link>
          </li>
        </div>
        )}
      </nav>
    </div>
  );
}

export default NavBar;