import React, {useState} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Card, Error, Input, Button } from "./AuthForms";

const Chance = (props) => {
  const { authJWT } = useAuth();
  const chance = JSON.parse(localStorage.getItem("chance"));
  const [chanceText, setChanceText] = useState(chance.text);
  const [action, setAction] = useState(chance.action);
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const referer = (props.location.state && props.location.state.referer) || '/chances';

  const addChance = async () => {
    if (chanceText.length === 0 ) {setError("Text is required")}
    else if (action.length === 0 ) {setError("Action is required")}
    else {
      try {
        chance.text = chanceText;
        chance.action = action;
        if (chance._id) {
          const result = await BoardService.putChance(chance, authJWT.jwt);
          if (result && (result.status === 200)) {
            setError();
            setReload(true);
          } else {
            setError((result.body && result.body.errors) || "Could not modify Chance");
          }
        } else {
          const result = await BoardService.addChance(chance, authJWT.jwt);
          if (result && (result.status === 201)) {
            setError();
            setReload(true);
          } else {
            setError((result.body && result.body.errors) || "Could not add Chance");
          }  
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  };

  if (reload) {
    return <Redirect to={referer} />;
  };

  return(
    <Card>
      { chance._id ? (
      <h3>Chance&nbsp;{chance._id}</h3>
      ) : (
        <h3>Add New Chance</h3>
      )}
    { error && <Error>{error}</Error> }
        <table>
          <tbody>
            <tr>
              <td>Text</td>
              <td>
                <Input
                  type="text"
                  value={chanceText}
                  onChange={e => {
                    setChanceText(e.target.value);
                  }}
                  placeholder={chanceText}
                />
              </td>
            </tr>
            <tr>
              <td>Action</td>
              <td>
                <Input
                  type="text"
                  value={action}
                  onChange={e => {
                    setAction(e.target.value);
                  }}
                  placeholder={action}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <Button onClick={() => {addChance()}}>OK</Button>
      </Card>
  )
};

export default Chance;