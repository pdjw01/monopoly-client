import axios from 'axios';
import auth_header from './auth-header';
const API_URL = auth_header().API_URL;
const authHeader = auth_header().authHeader;

class BoardService {
  getBoardFiles(jwt) {
    return axios.get(`${API_URL}boardFiles`, authHeader(jwt));
  }

  addBoardFile(boardFile, jwt) {
    return axios.get(`${API_URL}boardFiles/${boardFile}`, authHeader(jwt));
  }

  getBoards(jwt) {
    return axios.get(`${API_URL}boards`, authHeader(jwt));
  }

  deleteBoard(boardid, jwt) {
    return axios.delete(`${API_URL}boards/${boardid}`, authHeader(jwt));
  }

  getTokenImages(jwt) {
    return axios.get(`${API_URL}tokenImages`, authHeader(jwt));
  }

  getTokenImage(filename, jwt) {
    return axios.get(`${API_URL}tokenImages/${filename}`, authHeader(jwt));
  }

  getTokens(jwt) {
    return axios.get(`${API_URL}tokens`, authHeader(jwt));
  }

  getToken(tokenid, jwt) {
    return axios.get(`${API_URL}tokens/${tokenid}`, authHeader(jwt));
  }

  getTokensOnBoard(boardid, jwt) {
    return axios.get(`${API_URL}tokens?board=${boardid}`, authHeader(jwt));
  }

  addToken(token, jwt) {
    return axios.post(`${API_URL}tokens`, token, authHeader(jwt));
  }

  putToken(token, jwt) {
    return axios.put(`${API_URL}tokens/${token._id}`, token, authHeader(jwt));
  }

  deleteToken(tokenid, jwt) {
    return axios.delete(`${API_URL}tokens/${tokenid}`, authHeader(jwt));
  }

  getTitleDeeds(jwt) {
    return axios.get(`${API_URL}titleDeeds`, authHeader(jwt));
  }

  getTitleDeedsOnBoard(boardid, jwt) {
    return axios.get(`${API_URL}titleDeeds?board=${boardid}`, authHeader(jwt));
  }

  addTitleDeed(titleDeed, jwt) { //ToDo
    return axios.post(`${API_URL}titleDeeds`, titleDeed, authHeader(jwt));
  }

  putTitleDeed(titleDeed, jwt) {
    return axios.put(`${API_URL}titleDeeds/${titleDeed._id}`, titleDeed, authHeader(jwt));
  }

  deleteTitleDeed(titleDeedid, jwt) {
    return axios.delete(`${API_URL}titleDeeds/${titleDeedid}`, authHeader(jwt));
  }

  getChances(jwt) {
    return axios.get(`${API_URL}chances`, authHeader(jwt));
  }

  getChancesOnBoard(boardid, jwt) {
    return axios.get(`${API_URL}chances?board=${boardid}`, authHeader(jwt));
  }

  addChance(chance, jwt) {
    return axios.post(`${API_URL}chances`, chance, authHeader(jwt));
  }

  putChance(chance, jwt) {
    return axios.put(`${API_URL}chances/${chance._id}`, chance, authHeader(jwt));
  }

  deleteChance(chanceid, jwt) {
    return axios.delete(`${API_URL}chances/${chanceid}`, authHeader(jwt));
  }

  getCommunityChests(jwt) {
    return axios.get(`${API_URL}communityChests`, authHeader(jwt));
  }

  getCommunityChestsOnBoard(boardid, jwt) {
    return axios.get(`${API_URL}communityChests?board=${boardid}`, authHeader(jwt));
  }

  addCommunityChest(communityChest, jwt) {
    return axios.post(`${API_URL}communityChests`, communityChest, authHeader(jwt));
  }

  putCommunityChest(communityChest, jwt) {
    return axios.put(`${API_URL}communityChests/${communityChest._id}`, communityChest, authHeader(jwt));
  }

  deleteCommunityChest(communityChestid, jwt) {
    return axios.delete(`${API_URL}communityChests/${communityChestid}`, authHeader(jwt));
  }

  getPlayer(playerid, jwt){
    return axios.get(`${API_URL}players/${playerid}`, authHeader(jwt))
  }

  getMyPlayers(userid, jwt) {
    return axios.get(`${API_URL}players?user=${userid}`, authHeader(jwt))
  }

  addPlayer(player, jwt){
    return axios.post(`${API_URL}players`, player, authHeader(jwt))
  }

  deletePlayer(playerid, jwt) {
    return axios.delete(`${API_URL}players/${playerid}`, authHeader(jwt))
  }

  getGame(gameid, jwt) {
    return axios.get(`${API_URL}games/${gameid}`, authHeader(jwt));
  }

  getGamesByGame(gameid, jwt) {
    return axios.get(`${API_URL}games?game=${gameid}`, authHeader(jwt));
  }

  getCreatedGames(jwt) {
    return axios.get(`${API_URL}games?status=created`, authHeader(jwt));
  }

  addGame(gameName, board, user, token, jwt) {
    return axios.post(`${API_URL}games`, { gameName, board, user, token }, authHeader(jwt))
  }

  deleteGame(gameid, jwt) {
    return axios.delete(`${API_URL}games/${gameid}`, authHeader(jwt))
  }

}

export default new BoardService();