import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth } from "./context/auth";

function PrivateRoute({ component: Component, permission, ...rest }) {
  const { authJWT } = useAuth();
  return(
    <Route {...rest} render={(props) => (
      (!authJWT) ? (  // There is no user authenticated - redirect to login page
        <Redirect 
        to={{ pathname: "/login", state: { referer: props.location } }}
        />
      ) : (  // User is authenticated and has correct priviledge
        Number(permission) & authJWT.permissionLevel ? (
          <Component {...props} />
        ) : (  // User is authenticated but does not have correct priviledge
          <Redirect
          to={{ pathname: "/", state: { referer: props.location } }}
          />
        )
      )
    )}
    />
  )
}

export default PrivateRoute;