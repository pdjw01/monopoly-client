import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { useAuth } from "../context/auth";
import { Error, Button } from "./AuthForms";

const MyGames = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState([]);
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);

  useEffect(() => { 
    setReload(false);
    const getGames = async () => {
      try {
        const allboards = await BoardService.getBoards(authJWT.jwt);
        if (allboards && allboards.status===200) {
          console.log(allboards.data)
          setError();
        }
        const games = await BoardService.getCreatedGames(authJWT.jwt);
        if (games && games.status===200) {
          const myGames = await Promise.all(games.data.map(async (myGame) => {
            myGame.tokenList = await Promise.all(myGame.tokens.map(async (token) => {
              console.log(token);
              const newToken = await BoardService.getToken(token, authJWT.jwt);
              if (newToken && newToken.status===200) {
                return newToken.data
              } else {
                setError(`Token ${token} does not exist`)
              }
            }));
            console.log(myGame);
            const playerid = await myGame.players.find(player => player.user === authJWT.userid);
            console.log(playerid)
            if (!playerid) {
              myGame.joined = '0';
              myGame.tokenId = '0';
              myGame.tokenName = '';
              myGame.tokenImage = '';    
            }
            myGame.boardName = await allboards.data.find(board => board._id === myGame.board).version 
            console.log(myGame);
            return myGame;
          }));
          console.log(myGames);
          setContent(myGames);
        } else {
          setError('games error');
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getGames();
  }, [authJWT.userid, authJWT.jwt, reload]);

  const editTokenName = async (e, gameid) => {
    const games = await Promise.all(content.map(async game => {
      if (game._id === gameid){
        game.token = e.target.value
        console.log(game);
        const token = await game.tokenList.find(token => token._id === game.token);
        console.log(token);
        game.tokenId = token._id;
        game.tokenName = token.tokenName;
        game.tokenImage = token.image;
        return game;
      }
    }));
    console.log(content);
    console.log(games);
    setContent(games);
  };

  const joinGame = async(game) => {
    try{
      const result = await BoardService.addPlayer({game: game._id, user: authJWT.userid, token: game.token}, authJWT.jwt);
      if (result && result.status===201) {
        setError();
        setReload(true);
      } else {
        setError("Cannot add game");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  const leaveGame = async(playerid) => {
    try {
      const result = await BoardService.deletePlayer(playerid, authJWT.jwt);
      if (result && result.status===204) {
        setError();
        setReload(true);
      } else {
        setError("Cannot leave game");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  const deleteGame = async(gameid) => {
    try {
      const result = await BoardService.deleteGame(gameid, authJWT.jwt);
      if (result && result.status===204) {
        setError();
        setReload(true);
      } else {
        setError("Cannot delete game");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  if (content) {
    return(
        <div className="container">
        <header className="jumbotron">
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Game Name</th>
                <th scope="col">Board</th>
                <th scope="col">Token</th>
                <th scope="col">Image</th>
                <th scope="col">Join/Leave</th>
                {((authJWT.permissionLevel & 12) > 0) && (
                  <th scope="col">Delete</th>
                )}
              </tr>
            </thead>
            <tbody>
              {content.map((game) => {
                if (game.joined==="0") {
                  return(
                    <tr key={game._id}>
                      <td>{game.gameName}</td>
                      <td>{game.boardName}</td>
                      <td>
                        <select defaultValue={game.tokenId}
                          value={game.tokenId}
                          onChange={e => {
                            editTokenName(e, game._id);
                          }}>
                            <option value={'0'} key={'0'}>Select Token</option>
                          {game.tokenList.map((t) => {
                            return (
                              <option value={t._id} key={t._id}>{t.tokenName}</option>
                            )
                          })}
                        </select>
                      </td>
                      <td>
                        {game.tokenImage && (<img src={`data:image/png;base64,${game.tokenImage}`} alt={game.tokenName}/>)}
                      </td>
                      <td>
                        {game.joined==="0"?(
                          <Button onClick={() => {joinGame(game)}}>Join Game</Button>
                        ):(
                          <Button onClick={() => {leaveGame(game._id)}}>Leave Game</Button>
                        )}
                        </td>
                      {((authJWT.permissionLevel & 12) > 0) && (
                        <td>
                          <Button onClick={() => {deleteGame(game.game)}}>Delete</Button>
                        </td>
                      )}
                    </tr>
                  )
                } else {
                  return null;
                }
              })}
            </tbody>
          </table>
        </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  };
};

export default MyGames;