import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Error, Button } from "./AuthForms";

const TitleDeeds = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState([]);
  const board = JSON.parse(localStorage.getItem("board"));
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const [showTitleDeed, setShowTitleDeed] = useState(false);

  useEffect(() => {
    setReload(false);
    const getTitleDeeds = async () => {
      try {
        const result = await BoardService.getTitleDeedsOnBoard(board._id, authJWT.jwt);
        if (result && result.status===200) {
          setContent(result.data)
          setError();
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getTitleDeeds();
  }, [board._id, authJWT.jwt, reload]);

  const addTitleDeed = () => {
    const titleDeed = {
      board: board._id,
      propertyName: "Property Name",
      group: "Group",
      type: "Type",
      boardPos: 0,
      salePrice: 0,
      housePrice: 0,
      mortgageValue: 0,
      rent: {
        zero: 0,
        one: 0,
        two: 0,
        three: 0,
        four: 0,          
        hotel: 0,          
      }
      
    }
    localStorage.setItem("titleDeed", JSON.stringify(titleDeed));
    setShowTitleDeed(true);
  };

  const editTitleDeed = (titleDeed) => {
    localStorage.setItem("titleDeed", JSON.stringify(titleDeed));
    setShowTitleDeed(true);
  }

  const deleteTitleDeed = async(titleDeedId) => {
    try {
      const result = await BoardService.deleteTitleDeed(titleDeedId, authJWT.jwt);
      if (result && result.status===204) {
        setReload(true);
        setError();
      } else {
        setError("Cannot delete title deed");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  if (showTitleDeed) {
    return(
      <Redirect 
        to={{ pathname: "/titleDeed", state: { referer: props.location } }}
      />
    );
  };

  if (content) {
    return(
      <div className="container">
        <header className="jumbotron">
          <h3>Title Deeds for {board.version}</h3>
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Title Deed Name</th>
                <th scope="col">Group</th>
                <th scope="col">Type</th>
                <th scope="col">Board Pos</th>
                <th scope="col">Sale Price</th>
                <th scope="col">House Price</th>
                <th scope="col">Mortgage Value</th>
                <th scope="col">Rent Zero</th>
                <th scope="col">Rent One</th>
                <th scope="col">Rent Two</th>
                <th scope="col">Rent Three</th>
                <th scope="col">Rent Four</th>
                <th scope="col">Rent Hotel</th>
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Edit</th>
                )}
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Delete</th>
                )}
              </tr>
            </thead>
            <tbody>
              {content.map((titleDeed) => {
                return(
                  <tr key={titleDeed._id}>
                    <td>{titleDeed.propertyName}</td>
                    <td>{titleDeed.group}</td>
                    <td>{titleDeed.type}</td>
                    <td>{titleDeed.boardPos}</td>
                    <td>{titleDeed.salePrice}</td>
                    <td>{titleDeed.housePrice}</td>
                    <td>{titleDeed.mortgageValue}</td>
                    <td>{titleDeed.rent.zero}</td>
                    <td>{titleDeed.rent.one}</td>
                    <td>{titleDeed.rent.two}</td>
                    <td>{titleDeed.rent.three}</td>
                    <td>{titleDeed.rent.four}</td>
                    <td>{titleDeed.rent.hotel}</td>
                    {((authJWT.permissionLevel & 8) > 0) && (
                    <td>
                      <Button onClick={() => {editTitleDeed(titleDeed)}}>Edit</Button>
                    </td>
                    )}
                    {((authJWT.permissionLevel & 8) > 0) && (
                      <td>
                        <Button onClick={() => {deleteTitleDeed(titleDeed._id)}}>Delete</Button>
                      </td>
                    )}
                    </tr>
                )
              })}
            </tbody>
          </table>
          {((authJWT.permissionLevel & 8) > 0) && (
          <Button onClick={addTitleDeed}>Add</Button>
          )}
          </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  };
};

export default TitleDeeds;