import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import logoImg from "../img/mon1.png";
import { Card, Logo, Form, Button, Error } from "./AuthForms";
import { useAuth } from "../context/auth";
import UserService from "../services/UserService"; 

const Checkbox = props => (
  <input type="checkbox" {...props} />
)

const Roles = (props) => {
  const { authJWT } = useAuth();
  const [isChanged, setChanged] = useState(false);
  const [error, setError] = useState("");
  const user = JSON.parse(localStorage.getItem("display"));
  const [player, setPlayer] = useState(user.roles.includes("Player"));
  const [enhanced, setEnhanced] = useState(user.roles.includes("Enhanced"));
  const [admin, setAdmin] = useState(user.roles.includes("Admin"));
  const [superuser, setSuperuser] = useState(user.roles.includes("Superuser"));
  const referer = (props.location.state && props.location.state.referer) || '/';

  const postRoles = async () => {
    var permissionLevel = (player && 1) + (enhanced && 2) + (admin && 4) + (superuser && 8);
    try {
      const result = await UserService.changeRoles(user._id, permissionLevel, authJWT.jwt);
        if (result && (result.status === 200)) {
          setChanged(true);
          setError();
        } else {
          setError((result.body && result.body.errors) || "Could not change roles");
        }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  }
 
  if (isChanged) {
    return <Redirect to={referer} />;
  }
  return (
    <Card>
      <Logo src={logoImg} />
      <h3>{user.username}</h3>
      <Form>
        <label>
          <Checkbox
            checked={player}
            onChange={e => {
              setPlayer(e.target.checked);
            }}
          />
          <span>Player</span>
        </label>

        <label>
          <Checkbox
            checked={enhanced}
            onChange={e => {
              setEnhanced(e.target.checked);
            }}
          />
          <span>Enhanced</span>
        </label>

        <label>
          <Checkbox
            checked={admin}
            onChange={e => {
              setAdmin(e.target.checked);
            }}
          />
          <span>Admin</span>
        </label>

        <label>
          <Checkbox
            checked={superuser}
            onChange={e => {
              setSuperuser(e.target.checked);
            }}
          />
          <span>Superuser</span>
        </label>
        <Button onClick={postRoles}>Change Roles</Button>
      </Form>
      { error &&<Error>{error}</Error> }
    </Card>
  );
}

export default Roles;