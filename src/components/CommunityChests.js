import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Error, Button } from "./AuthForms";

const CommunityChests = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState([]);
  const board = JSON.parse(localStorage.getItem("board"));
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const [showCommunityChest, setShowCommunityChest] = useState(false);

  useEffect(() => {
    setReload(false);
    const getCommunityChests = async () => {
      try {
        const result = await BoardService.getCommunityChestsOnBoard(board._id, authJWT.jwt);
        if (result && result.status===200) {
          setError();
          setContent(result.data)
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getCommunityChests();
  }, [board._id, authJWT.jwt, reload]);

  const addCommunityChest = () => {
    const communityChest = {
      board: board._id,
      text: "CommunityChest Text",
      action: "What the communityChest card will do"
    }
    localStorage.setItem("communityChest", JSON.stringify(communityChest));
    setShowCommunityChest(true);
  };

  const editCommunityChest = (communityChest) => {
    localStorage.setItem("communityChest", JSON.stringify(communityChest));
    setShowCommunityChest(true);
  }

  const deleteCommunityChest = async(communityChestid) => {
    try {
      const result = await BoardService.deleteCommunityChest(communityChestid, authJWT.jwt);
      if (result && result.status===204) {
        setError();
        setReload(true);
      } else {
        setError("Cannot delete communityChest");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  if (showCommunityChest) {
    return(
      <Redirect 
        to={{ pathname: "/communityChest", state: { referer: props.location } }}
      />
    );
  };

  if (content) {
    return(
      <div className="container">
        <header className="jumbotron">
          <h3>CommunityChests for {board.version}</h3>
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Text</th>
                <th scope="col">Action</th>
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Edit</th>
                )}
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Delete</th>
                  )}
              </tr>
            </thead>
            <tbody>
              {content.map((communityChest) => {
                return(
                  <tr key={communityChest._id}>
                    <td>{communityChest.text}</td>
                    <td>{communityChest.action}</td>
                    {((authJWT.permissionLevel & 8) > 0) && (
                    <td>
                      <Button onClick={() => {editCommunityChest(communityChest)}}>Edit</Button>
                    </td>
                    )}
                    {((authJWT.permissionLevel & 8) > 0) && (
                    <td>
                      <Button onClick={() => {deleteCommunityChest(communityChest._id)}}>Delete</Button>
                    </td>
                    )}
                    </tr>
                )
              })}
            </tbody>
          </table>
          {((authJWT.permissionLevel & 8) > 0) && (
          <Button onClick={addCommunityChest}>Add</Button>
          )}
          </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  };
};

export default CommunityChests;