import React, {useState} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Card, Error, Input, Button } from "./AuthForms";

const CommunityChest = (props) => {
  const { authJWT } = useAuth();
  const communityChest = JSON.parse(localStorage.getItem("communityChest"));
  const [communityChestText, setCommunityChestText] = useState(communityChest.text);
  const [action, setAction] = useState(communityChest.action);
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const referer = (props.location.state && props.location.state.referer) || '/communityChests';

  const addCommunityChest = async () => {
    if (communityChestText.length === 0 ) {setError("Text is required")}
    else if (action.length === 0 ) {setError("Action is required")}
    else {
      try {
        communityChest.text = communityChestText;
        communityChest.action = action;
        if (communityChest._id) {
          const result = await BoardService.putCommunityChest(communityChest, authJWT.jwt);
          if (result && (result.status === 200)) {
            setError();
            setReload(true);
          } else {
            setError((result.body && result.body.errors) || "Could not modify CommunityChest");
          }
        } else {
          const result = await BoardService.addCommunityChest(communityChest, authJWT.jwt);
          if (result && (result.status === 201)) {
            setError();
            setReload(true);
          } else {
            setError((result.body && result.body.errors) || "Could not add CommunityChest");
          }  
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  };

  if (reload) {
    return <Redirect to={referer} />;
  };

  return(
    <Card>
      { communityChest._id ? (
      <h3>CommunityChest&nbsp;{communityChest._id}</h3>
      ) : (
        <h3>Add New CommunityChest</h3>
      )}
    { error && <Error>{error}</Error> }
        <table>
          <tbody>
            <tr>
              <td>Text</td>
              <td>
                <Input
                  type="text"
                  value={communityChestText}
                  onChange={e => {
                    setCommunityChestText(e.target.value);
                  }}
                  placeholder={communityChestText}
                />
              </td>
            </tr>
            <tr>
              <td>Action</td>
              <td>
                <Input
                  type="text"
                  value={action}
                  onChange={e => {
                    setAction(e.target.value);
                  }}
                  placeholder={action}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <Button onClick={() => {addCommunityChest()}}>OK</Button>
      </Card>
  )
};

export default CommunityChest;