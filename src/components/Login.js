import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import logoImg from "../img/mon1.png";
import { Card, Logo, Form, Input, Button, Error } from "../components/AuthForms";
import { useAuth } from "../context/auth";
import UserService from "../services/UserService"; 

const Login = (props) => {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [error, setError] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { setAuthJWT } = useAuth();
  const referer = (props.location.state && props.location.state.referer) || '/';

  const postLogin = async () => {
    if (username.length === 0 ) {setError("Username is required")}
    else if (password.length === 0 ) {setError("Password is required")}
    else {
      try {
        const result = await UserService.login(username, password);
        if (result && (result.status === 200)) {
          result.data.username = username;
          setAuthJWT(result.data);
          setLoggedIn(true);
        } else {
          setError((result.body && result.body.errors) || "Could not log in");
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  }
 
  if (isLoggedIn) {
    return <Redirect to={referer} />;
  }

  return (
    <Card>
      <Logo src={logoImg} />
      <Form>
        <Input
          type="username"
          value={username}
          onChange={e => {
            setUsername(e.target.value);
          }}
          placeholder="username"
        />
        <Input
          type="password"
          value={password}
          onChange={e => {
            setPassword(e.target.value);
          }}
          placeholder="password"
        />
        <Button onClick={postLogin}>Sign In</Button>
      </Form>
      { error &&<Error>{error}</Error> }
    </Card>
  );
}

export default Login;