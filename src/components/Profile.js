import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import { useAuth } from "../context/auth";
import logoImg from "../img/mon1.png";
import { Card, Logo } from "../components/AuthForms";
import { Redirect } from "react-router-dom";

const Profile = (props) => {
  const { authJWT } = useAuth();
  const [currentUser, setCurrentUser] = useState();
  const [changePass, setChangePass] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    const getCurrentUser = async () => {
      try {
        const result = await UserService.getUserByUserid(authJWT.userid, authJWT.jwt);
        if (result && result.status===200) {
          result.data.roles = UserService.permission2roles(result.data.permissionLevel);
          setCurrentUser(result.data);
          setError();
        }  
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getCurrentUser();
  }, [authJWT.userid, authJWT.jwt]);

  const changePassword = (user) => {
    localStorage.setItem("display", JSON.stringify(user));
    setChangePass(true);
  }

  if (changePass) {
    return(
      <Redirect 
        to={{ pathname: "/change-password", state: { referer: props.location } }}
      />
    );
  }

  if (currentUser) {
    return(
      <Card>
        <Logo src={logoImg} />
        <h3>{currentUser.username}</h3>
        <p>
          <strong>Id:</strong>{" "}
          {currentUser._id}
        </p>
        <strong>Roles:</strong>
        <ul>
          {currentUser.roles &&
            currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
        </ul>
        <button onClick={() => {changePassword(currentUser)}}>Change Password</button>
      </Card>  
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  }
}

export default Profile;