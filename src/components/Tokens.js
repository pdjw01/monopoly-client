import React, {useState, useEffect} from "react";
import UserService from "../services/UserService";
import BoardService from "../services/BoardService";
import { Redirect } from "react-router-dom";
import { useAuth } from "../context/auth";
import { Error, Button } from "./AuthForms";

const Tokens = (props) => {
  const { authJWT } = useAuth();
  const [content, setContent] = useState([]);
  const board = JSON.parse(localStorage.getItem("board"));
  const [error, setError] = useState();
  const [reload, setReload] = useState(false);
  const [showToken, setShowToken] = useState(false);

  useEffect(() => {
    setReload(false);
    const getTokens = async () => {
      try {
        const result = await BoardService.getTokensOnBoard(board._id, authJWT.jwt);
        if (result && result.status===200) {
          setContent(result.data);
          setError();
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
    getTokens();
  }, [board._id, authJWT.jwt, reload]);

  const addToken = () => {
    const token = {
      board: board._id,
      tokenName: "Token Name",
      filename: "Battleship.png",
      image: ""
    }
    localStorage.setItem("token", JSON.stringify(token));
    setShowToken(true);
  };

  const editToken = (token) => {
    localStorage.setItem("token", JSON.stringify(token));
    setShowToken(true);
  }

  const deleteToken = async(tokenid) => {
    try {
      const result = await BoardService.deleteToken(tokenid, authJWT.jwt);
      if (result && result.status===204) {
        setReload(true);
        setError();
      } else {
        setError("Cannot delete token");
      }
    } catch (err) {
      setError(UserService.handleError(err));
    }
  };

  if (showToken) {
    return(
      <Redirect 
        to={{ pathname: "/token", state: { referer: props.location } }}
      />
    );
  };

  if (content) {
    return(
      <div className="container">
        <header className="jumbotron">
          <h3>Tokens for {board.version}</h3>
        { error && <Error>{error}</Error> }
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Token Name</th>
                <th scope="col">Filename</th>
                <th scope="col">Image</th>
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Edit</th>
                )}
                {((authJWT.permissionLevel & 8) > 0) && (
                  <th scope="col">Delete</th>
                  )}
              </tr>
            </thead>
            <tbody>
              {content.map((token) => {
                return(
                  <tr key={token._id}>
                    <td>{token.tokenName}</td>
                    <td>{token.filename}</td>
                    <td><img src={`data:image/png;base64,${token.image}`} alt={token.tokenName}/></td>
                    {((authJWT.permissionLevel & 8) > 0) && (
                    <td>
                      <Button onClick={() => {editToken(token)}}>Edit</Button>
                    </td>
                    )}
                    {((authJWT.permissionLevel & 8) > 0) && (
                    <td>
                      <Button onClick={() => {deleteToken(token._id)}}>Delete</Button>
                    </td>
                    )}
                    </tr>
                )
              })}
            </tbody>
          </table>
          {((authJWT.permissionLevel & 8) > 0) && (
          <Button onClick={addToken}>Add</Button>
          )}
          </header>
      </div>
    )
  } else if (error){
    return(
      <div>
        <h3>{error}</h3>
      </div>
    )
  } else {
    return(
      <div>
        <h3>Waiting...</h3>
      </div>
    )
  };
};

export default Tokens;