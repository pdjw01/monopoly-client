import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import logoImg from "../img/mon1.png";
import { Card, Logo, Form, Input, Button, Error } from './AuthForms';
import UserService from "../services/UserService"; 

const Signup = (props) => {
  const [signedup, setSignedup] = useState(false);
  const [error, setError] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [again, setAgain] = useState("");
  const referer = (props.location.state && props.location.state.referer) || '/';

  const postSignup = async () => {
    if (username.length === 0 ) {setError("Username is required")}
    else if (password.length === 0 ) {setError("Password is required")}
    else if (password !== again ) {setError("Passwords dont match")}
    else {
      try {
        const result = await UserService.addUser(username, password);
        if (result && result.status === 201) {
          setSignedup(true);
          setError();
        } else {
          setError((result.body && result.body.errors) || "Could not add user");
        }
      } catch (err) {
        setError(UserService.handleError(err));
      }
    }
  }
 
  if (signedup) {
    return <Redirect to={referer} />;
  }

  return (
    <Card>
      <Logo src={logoImg} />
      <h3>Sign Up</h3>
      <Form>
      <Input
          type="username"
          value={username}
          onChange={e => {
            setUsername(e.target.value);
          }}
          placeholder="username"
        />
        <Input
          type="password"
          value={password}
          onChange={e => {
            setPassword(e.target.value);
          }}
          placeholder="password"
        />
        <Input
          type="password"
          value={again}
          onChange={e => {
            setAgain(e.target.value);
          }}
          placeholder="password again"
        />
        <Button onClick={postSignup}>Sign Up</Button>
      </Form>
      { error &&<Error>{error}</Error> }
    </Card>
  );
}

export default Signup;